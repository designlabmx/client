# <img src="assets/icons/icon-white-android.png" width="64" height="64" /> Extinction Email Client

![Current version](https://img.shields.io/badge/dynamic/yaml?label=Current%20version&query=version&url=https%3A%2F%2Fgitlab.com%2Fextinction-email%2Fclient%2Fraw%2Fmain%2Fpubspec.yaml%3Finline%3Dfalse&style=for-the-badge&logo=flutter&logoColor=white) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/extinction-email/client/main?style=for-the-badge)

A privacy-oriented, end-to-end encrypted Email client focused on simplicity and easy use.

(The README is unfortunately not ready yet. We will update it soon.)

## Download

Extinction.Email is natively compatible to almost any operating system. Anyway, not for all of them automated build are available yet. Below you find the current ones.

- Android
	- [Download APK](https://gitlab.com/extinction-email/client/-/jobs/artifacts/main/browse?job=build%3Aapk)
- Linux
	- [Download for Debian-bases systems](https://gitlab.com/extinction-email/client/-/jobs/artifacts/main/browse?job=build%3Adebian)
	- [Download for generic Linux](https://gitlab.com/extinction-email/client/-/jobs/artifacts/main/download?job=build%3Alinux)

## Messages

Messages are rendered as plain text or basic html. The html rendering is done without any kind of webview using only a basic but performant renderer.

## Encryption

Extinction Email uses PGP to encrypt your emails.

## License

Extinction Email is EUPL-1.2 licensed. See [LICENSE](LICENSE) for further details.
