import 'dart:async';

import 'package:client/pages/key_setup_page.dart';
import 'package:client/widgets/main_secondary.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/setup';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  TextEditingController usernameController = TextEditingController();

  TextEditingController imapHostController =
      TextEditingController(text: 'mail.for-futu.re');
  TextEditingController smtpHostController =
      TextEditingController(text: 'mail.for-futu.re');
  TextEditingController imapPortController =
      TextEditingController(text: 993.toString());
  TextEditingController smtpPortController =
      TextEditingController(text: 587.toString());

  int _selectedPage = 0;

  Completer<LoginPageArguments> argumentsCompleter;

  GlobalKey<FormState> _mailFormKey = GlobalKey<FormState>();

  bool _showPasswords = false;

  bool _connectionError = false;

  @override
  void initState() {
    argumentsCompleter = Completer();
    if (!argumentsCompleter.isCompleted)
      argumentsCompleter.future.then((value) {
        if (value?.errorInConnection ?? false) {
          _connectionError = true;
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Could not sign in automatically.'),
          ));
        }
      });
    initializeFormFields();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!argumentsCompleter.isCompleted)
      argumentsCompleter.complete(ModalRoute.of(context).settings.arguments);
    return Scaffold(
      body: Container(
        padding: (MediaQuery.of(context).size.height > 350)
            ? EdgeInsets.only(top: MediaQuery.of(context).size.height / 8)
            : EdgeInsets.zero,
        alignment: Alignment.center,
        child: MainSecondary(
          main: Container(
            constraints: BoxConstraints(maxWidth: 768),
            alignment: Alignment.topCenter,
            child: Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Wrap(
                    alignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    runSpacing: 16,
                    spacing: 16,
                    children: [
                      Hero(
                          tag: 'logo',
                          child: Image.asset(
                              'assets/icons/icon-white-android.png',
                              scale: 4)),
                      Hero(
                        tag: 'mainHeadline',
                        child: Text(
                          'extinction.email',
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    'Your privacyful and encrypted email client',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Divider(),
                  Text('Select how to start'),
                  Divider(
                    indent: MediaQuery.of(context).size.width / 3,
                    endIndent: MediaQuery.of(context).size.width / 3,
                  ),
                  ListTile(
                    title: Text('Use existing mail account'),
                    subtitle:
                        Text('May be an extinction.email account or any third'),
                    trailing: Icon(Icons.keyboard_arrow_right),
                    onTap: () => setState(() => _selectedPage = 1),
                  ),
                ],
              ),
            ),
          ),
          secondary: _selectedPage == 0
              ? null
              : Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: _mailFormKey,
                  child: Container(
                    constraints: BoxConstraints(maxWidth: 768),
                    alignment: Alignment.topCenter,
                    child: Card(
                      child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: 3,
                        itemBuilder: (c, i) => Padding(
                          child: [
                            Text('Log in to your email account'),
                            Wrap(
                              runSpacing: 16,
                              spacing: 16,
                              alignment: WrapAlignment.spaceEvenly,
                              children: [
                                Container(
                                  constraints: BoxConstraints(maxWidth: 320),
                                  child: TextFormField(
                                    validator: (value) {
                                      return (RegExp(emailRegex)
                                              .hasMatch(value))
                                          ? null
                                          : 'This is not a valid email address.';
                                    },
                                    autofocus: true,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Email address',
                                        prefixIcon: Icon(Icons.email)),
                                    controller: emailController,
                                    keyboardType: TextInputType.emailAddress,
                                  ),
                                ),
                                Container(
                                  constraints: BoxConstraints(maxWidth: 320),
                                  child: TextFormField(
                                    validator: (value) => value.isEmpty
                                        ? 'Please provide a password'
                                        : null,
                                    decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Account password',
                                        prefixIcon: Icon(Icons.vpn_key),
                                        suffixIcon: IconButton(
                                          tooltip: (_showPasswords
                                                  ? 'Hide'
                                                  : 'Show') +
                                              ' passwords',
                                          icon: Icon(_showPasswords
                                              ? Icons.visibility_off
                                              : Icons.visibility),
                                          onPressed: () => setState(() =>
                                              _showPasswords = !_showPasswords),
                                        )),
                                    controller: passwordController,
                                    keyboardType: TextInputType.visiblePassword,
                                    obscureText: !_showPasswords,
                                  ),
                                ),
                                if (_connectionError)
                                  Container(
                                    constraints: BoxConstraints(maxWidth: 320),
                                    child: TextFormField(
                                      validator: (value) {
                                        const hostRegex =
                                            r"^[a-zA-Z0-9-\.]+\.[a-zA-Z]+";
                                        return (RegExp(hostRegex)
                                                .hasMatch(value))
                                            ? null
                                            : 'This is not a valid server name.';
                                      },
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: 'IMAP host',
                                          prefixIcon: Icon(Icons.domain)),
                                      controller: imapHostController,
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                  ),
                                if (_connectionError)
                                  Container(
                                    constraints: BoxConstraints(maxWidth: 320),
                                    child: TextFormField(
                                      validator: (value) => value.isEmpty
                                          ? 'Please provide a number (993)'
                                          : null,
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: 'IMAP port',
                                          prefixIcon: Icon(Icons.dialpad)),
                                      controller: imapPortController,
                                      keyboardType: TextInputType.number,
                                    ),
                                  ),
                                if (_connectionError)
                                  Container(
                                    constraints: BoxConstraints(maxWidth: 320),
                                    child: TextFormField(
                                      validator: (value) {
                                        const hostRegex =
                                            r"^[a-zA-Z0-9-\.]+\.[a-zA-Z]+";
                                        return (RegExp(hostRegex)
                                                .hasMatch(value))
                                            ? null
                                            : 'This is not a valid server name.';
                                      },
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: 'SMTP host',
                                          prefixIcon: Icon(Icons.domain)),
                                      controller: smtpHostController,
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                  ),
                                if (_connectionError)
                                  Container(
                                    constraints: BoxConstraints(maxWidth: 320),
                                    child: TextFormField(
                                      validator: (value) => value.isEmpty
                                          ? 'Please provide a number (587)'
                                          : null,
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(),
                                          labelText: 'SMTP port',
                                          prefixIcon: Icon(Icons.dialpad)),
                                      controller: smtpPortController,
                                      keyboardType: TextInputType.number,
                                    ),
                                  ),
                              ],
                            ),
                            ButtonBar(
                              children: [
                                OutlineButton(
                                  child: Text('Next'),
                                  onPressed: () {
                                    if (_mailFormKey.currentState.validate())
                                      nextPage();
                                  },
                                )
                              ],
                            )
                          ][i],
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        ),
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Future<void> nextPage() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('user', emailController.text);
    prefs.setString('password', passwordController.text);

    if (imapHostController.text.isNotEmpty)
      prefs.setString('imapHost', imapHostController.text);
    if (imapPortController.text.isNotEmpty)
      prefs.setInt('imapPort', int.parse(imapPortController.text));
    if (smtpHostController.text.isNotEmpty)
      prefs.setString('smtpHost', smtpHostController.text);
    if (smtpPortController.text.isNotEmpty)
      prefs.setInt('smtpPort', int.parse(smtpPortController.text));

    Navigator.of(context).pushNamed(KeySetupPage.routeName);
  }

  Future<void> initializeFormFields() async {
    final prefs = await SharedPreferences.getInstance();
    emailController.text = prefs.getString('user') ?? '';
    passwordController.text = prefs.getString('password') ?? '';
  }
}

class LoginPageArguments {
  final bool errorInConnection;

  LoginPageArguments({this.errorInConnection = false});
}

const emailRegex =
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+";
