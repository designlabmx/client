import 'dart:async';

import 'package:client/pages/compose_message.dart';
import 'package:client/pages/login_page.dart';
import 'package:client/widgets/full_message_view.dart';
import 'package:client/widgets/mailbox_tree_drawer.dart';
import 'package:client/widgets/message_tile.dart';
import 'package:client/widgets/responsive_list.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:responsive_scaffold/responsive_scaffold.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InboxPage extends StatefulWidget {
  static const routeName = '/inbox';
  @override
  _InboxPageState createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage> {
  List<MailClient> mailClients = [];
  Map<String, Tree<Mailbox>> mailboxes = {};
  List<MimeMessage> messages = [];

  int selectedMessage = -1;

  int selectedMailClient = 0;

  Mailbox selectedMailbox;

  EventBus _eventBus = EventBus();

  @override
  void initState() {
    setupAccount();
    _eventBus.on<MailLoadEvent>().listen((event) {
      print(event.runtimeType);
      print(event);
      setState(() {
        messages.insert(0, event.message);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffold(
      body: ResponsiveList(
          unselectedDetail: Center(
            child: //Text('No message selected.'),

                Wrap(
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              runSpacing: 16,
              spacing: 16,
              children: [
                Hero(
                    tag: 'logo',
                    child: Image.asset('assets/icons/icon-white-android.png',
                        scale: 4)),
                Hero(
                  tag: 'mainHeadline',
                  child: Text(
                    'extinction.email',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
              ],
            ),
          ),
          children: messages
              .map((e) => ResponsiveListTile(
                    child: MessageTile(
                      message: e,
                      active: selectedMessage == 1,
                    ),
                    detailsBuilder: (c) => FullMessageView(
                      message: e,
                    ),
                  ))
              .toList()),
      title: Text('Extinction.Email'),
      drawer: MailboxTreeDrawer.current(
          mailClients: mailClients,
          mailboxes: mailboxes,
          onMailboxChange: (Mailbox mailbox) {
            messages = [];
            setState(() {
              selectedMailbox = mailbox;
              loadMessages();
            });
          },
          onMailClientChange: (int i) {
            setState(() {
              selectedMailClient = i;
              setState(() {
                messages = [];
              });
              loadFolders().then((value) => loadMessages());
            });
          },
          initialMailbox: selectedMailbox,
          initialMailClient: selectedMailClient),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.edit),
        tooltip: 'Compose',
        onPressed: () => showComposeDialog(context: context),
      ),
    );
  }

  Future loadMessages() async {
    await mailClients[selectedMailClient].selectInbox();
    var fetchResponse = await mailClients[selectedMailClient]
        .fetchMessages(count: 30, mailbox: selectedMailbox);
    if (fetchResponse.isOkStatus) {
      messages.insertAll(0, fetchResponse.result);
      setState(() {});
    }
    mailClients[selectedMailClient].startPolling(Duration(seconds: 10));
  }

  Future loadFolders() async {
    for (MailClient mailClient in mailClients) {
      await mailClient.connect();
      MailResponse<Tree<Mailbox>> mailBoxesResponse =
          await mailClient.listMailboxesAsTree();
      if (mailBoxesResponse.isOkStatus) {
        mailboxes[mailClient.account.name] = mailBoxesResponse.result;
      }
    }
  }

  Future fetchAccountInformation() async {
    final prefs = await SharedPreferences.getInstance();
    String email = prefs.getString('user');
    String password = prefs.getString('password');

    MailAccount account;
    try {
      ClientConfig config =
          await Discover.discover(email, forceSslConnection: true);

      account =
          MailAccount.fromDiscoveredSettings('test', email, password, config);
    } catch (e) {
      print('Could not determinate automatic settings. Using manual ones...');
      String imapServerHost = prefs.getString('imapHost') ?? 'mail.for-futu.re';
      int imapServerPort = prefs.getInt('imapPort') ?? 993;
      String smtpServerHost = prefs.getString('smtpHost') ?? 'mail.for-futu.re';
      int smtpServerPort = prefs.getInt('smtpPort') ?? 587;

      var auth = PlainAuthentication(email, password);
      var incoming = MailServerConfig()
        ..authentication = auth
        ..serverConfig = ServerConfig(
            type: ServerType.imap,
            hostname: imapServerHost,
            port: imapServerPort,
            socketType: SocketType.ssl,
            authentication: Authentication.passwordEncrypted,
            usernameType: UsernameType.emailAddress);
      var outgoing = MailServerConfig()
        ..authentication = auth
        ..serverConfig = ServerConfig(
            type: ServerType.smtp,
            hostname: smtpServerHost,
            port: smtpServerPort,
            socketType: SocketType.starttls,
            authentication: Authentication.passwordEncrypted,
            usernameType: UsernameType.emailAddress);
      account = MailAccount()
        ..name = 'test'
        ..email = email
        ..incoming = incoming
        ..outgoing = outgoing;
    }
    mailClients = [];
    mailClients
        .add(MailClient(account, isLogEnabled: false, eventBus: _eventBus));
  }

  void setupAccount() async {
    try {
      await fetchAccountInformation();
      await loadFolders();
      await loadMessages();
      setState(() {});
    } catch (e) {
      Navigator.of(context).pushReplacementNamed(LoginPage.routeName,
          arguments: LoginPageArguments(errorInConnection: true));
    }
  }

  Future<void> logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('password');
    Navigator.of(context).pushReplacementNamed(LoginPage.routeName);
  }
}
