import 'dart:async';
import 'dart:convert';

import 'package:client/widgets/main_secondary.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class KeySetupPage extends StatefulWidget {
  static const routeName = '/encryption';
  @override
  _KeySetupPageState createState() => _KeySetupPageState();
}

class _KeySetupPageState extends State<KeySetupPage> {
  TextEditingController pgpBlockController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController repeatPasswordController = TextEditingController();

  TextEditingController usernameController = TextEditingController();

  TextEditingController imapHostController = TextEditingController();
  TextEditingController smptHostController = TextEditingController();
  TextEditingController imapPortController = TextEditingController();
  TextEditingController smptPortController = TextEditingController();

  int _selectedPage = 0;

  GlobalKey<FormState> _importFormKey = GlobalKey<FormState>();
  GlobalKey<FormState> _createFormKey = GlobalKey<FormState>();

  bool _showPasswords = false;

  PageController _secondaryPageController = PageController();

  static const String privateKeyRegex =
      r'^-----BEGIN PGP PRIVATE KEY BLOCK-----[\n\w+/=]*-----END PGP PRIVATE KEY BLOCK-----';

  String _secretKey;
  String _secretKeyPassword;

  Timer readKeyFromClipboard;

  @override
  void initState() {
    initializeFormFields();
    readKeyFromClipboard =
        Timer.periodic(Duration(milliseconds: 250), (timer) async {
      if (pgpBlockController.text.isEmpty) {
        ClipboardData data = await Clipboard.getData(Clipboard.kTextPlain);
        if (data != null &&
            data.text != null &&
            RegExp(privateKeyRegex).hasMatch(data.text.trim())) {
          pgpBlockController.text = data.text;
          readKeyFromClipboard.cancel();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content:
                Text('Automatically imported PGP secret key from clipboard.'),
          ));
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: (MediaQuery.of(context).size.height > 350)
            ? EdgeInsets.only(top: MediaQuery.of(context).size.height / 8)
            : EdgeInsets.zero,
        alignment: Alignment.center,
        child: MainSecondary(
            main: Container(
              constraints: BoxConstraints(maxWidth: 768),
              alignment: Alignment.topCenter,
              child: Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Setup mail encryption',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Divider(),
                    Text('Select how to start'),
                    Divider(
                      indent: MediaQuery.of(context).size.width / 3,
                      endIndent: MediaQuery.of(context).size.width / 3,
                    ),
                    ListTile(
                      title: Text('I am new to mail encryption'),
                      subtitle: Text(
                          'If you have never set up mail encryption before for this account.'),
                      trailing: Icon(Icons.keyboard_arrow_right),
                      onTap: () => setSecondaryPage(1),
                    ),
                    ListTile(
                      title: Text('Import existing mail encryption keys'),
                      subtitle: Text(
                          'If you already set up PGP encryption for this mail address and want to import an existing secret key.'),
                      trailing: Icon(Icons.keyboard_arrow_right),
                      onTap: () => setSecondaryPage(2),
                    ),
                  ],
                ),
              ),
            ),
            secondary: _selectedPage == 0
                ? null
                : PageView(
                    scrollDirection: Axis.vertical,
                    controller: _secondaryPageController,
                    children: [
                      Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        key: _createFormKey,
                        child: Container(
                          constraints: BoxConstraints(maxWidth: 768),
                          alignment: Alignment.topCenter,
                          child: Card(
                            child: ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: 3,
                              itemBuilder: (c, i) => Padding(
                                child: [
                                  Text('Create a new PGP key pair'),
                                  Wrap(
                                    runSpacing: 16,
                                    spacing: 16,
                                    alignment: WrapAlignment.spaceEvenly,
                                    children: [
                                      Container(
                                        constraints:
                                            BoxConstraints(maxWidth: 320),
                                        child: TextFormField(
                                          validator: (value) => value.isEmpty
                                              ? 'Please provide a password'
                                              : null,
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(),
                                              labelText:
                                                  'Encryption key password',
                                              prefixIcon: Icon(Icons.vpn_key),
                                              suffixIcon: IconButton(
                                                tooltip: (_showPasswords
                                                        ? 'Hide'
                                                        : 'Show') +
                                                    ' passwords',
                                                icon: Icon(_showPasswords
                                                    ? Icons.visibility_off
                                                    : Icons.visibility),
                                                onPressed: () => setState(() =>
                                                    _showPasswords =
                                                        !_showPasswords),
                                              )),
                                          controller: passwordController,
                                          keyboardType:
                                              TextInputType.visiblePassword,
                                          obscureText: !_showPasswords,
                                        ),
                                      ),
                                      Container(
                                        constraints:
                                            BoxConstraints(maxWidth: 320),
                                        child: TextFormField(
                                          validator: (value) =>
                                              value != passwordController.text
                                                  ? 'Passwords do not match.'
                                                  : null,
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(),
                                              labelText:
                                                  'Repeat encryption key password',
                                              prefixIcon: Icon(Icons.vpn_key),
                                              suffixIcon: IconButton(
                                                tooltip: (_showPasswords
                                                        ? 'Hide'
                                                        : 'Show') +
                                                    ' passwords',
                                                icon: Icon(_showPasswords
                                                    ? Icons.visibility_off
                                                    : Icons.visibility),
                                                onPressed: () => setState(() =>
                                                    _showPasswords =
                                                        !_showPasswords),
                                              )),
                                          controller: repeatPasswordController,
                                          keyboardType:
                                              TextInputType.visiblePassword,
                                          obscureText: !_showPasswords,
                                        ),
                                      ),
                                    ],
                                  ),
                                  ButtonBar(
                                    children: [
                                      OutlineButton(
                                        child: Text('Generate'),
                                        onPressed: () {
                                          if (_createFormKey.currentState
                                              .validate()) generateSecretKey();
                                        },
                                      )
                                    ],
                                  )
                                ][i],
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        key: _importFormKey,
                        child: Container(
                          constraints: BoxConstraints(maxWidth: 768),
                          alignment: Alignment.topCenter,
                          child: Card(
                            child: ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: 3,
                              itemBuilder: (c, i) => Padding(
                                child: [
                                  Text('Import existing PGP encryption key'),
                                  Wrap(
                                    runSpacing: 16,
                                    spacing: 16,
                                    alignment: WrapAlignment.spaceEvenly,
                                    children: [
                                      Container(
                                          constraints: BoxConstraints.tightFor(
                                            width: 320,
                                          ),
                                          child: RaisedButton(
                                            child: Text('Import from file'),
                                            onPressed: () async {
                                              try {
                                                FilePickerCross selectedFile =
                                                    await FilePickerCross
                                                        .importFromStorage();
                                                if (RegExp(privateKeyRegex)
                                                    .hasMatch(selectedFile
                                                        .toString())) {
                                                  pgpBlockController.text =
                                                      selectedFile.toString();
                                                } else {
                                                  showDialog(
                                                      context: context,
                                                      builder:
                                                          (c) => AlertDialog(
                                                                title: Text(
                                                                    'No PGP data found'),
                                                                content: Text(
                                                                    'The file you selected does not contain any valid PGP key. Please select an ASCII-armored PGP secret key.'),
                                                                actions: [
                                                                  FlatButton(
                                                                    child: Text(
                                                                        'Okay'),
                                                                    onPressed:
                                                                        Navigator.of(context)
                                                                            .pop,
                                                                  )
                                                                ],
                                                              ));
                                                }
                                              } catch (e) {
                                                showDialog(
                                                    context: context,
                                                    builder: (c) => AlertDialog(
                                                          title: Text(
                                                              'No file selected.'),
                                                          actions: [
                                                            FlatButton(
                                                              child:
                                                                  Text('Okay'),
                                                              onPressed:
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop,
                                                            )
                                                          ],
                                                        ));
                                              }
                                            },
                                          )),
                                      Container(
                                        constraints: BoxConstraints(
                                            maxWidth: 320,
                                            maxHeight: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.6),
                                        child: TextFormField(
                                          maxLines: null,
                                          minLines: 2,
                                          validator: (value) {
                                            return (RegExp(privateKeyRegex)
                                                    .hasMatch(value))
                                                ? null
                                                : 'Please provide a private key block.';
                                          },
                                          autofocus: true,
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(),
                                              labelText: 'ASCII PGP block',
                                              prefixIcon:
                                                  Icon(Icons.text_snippet)),
                                          controller: pgpBlockController,
                                          keyboardType: TextInputType.multiline,
                                        ),
                                      ),
                                      Container(
                                        constraints:
                                            BoxConstraints(maxWidth: 320),
                                        child: TextFormField(
                                          validator: (value) => null,
                                          decoration: InputDecoration(
                                              border: OutlineInputBorder(),
                                              labelText:
                                                  'Encryption key password',
                                              helperText:
                                                  'Leave empty if your key is not password protected',
                                              prefixIcon: Icon(Icons.vpn_key),
                                              suffixIcon: IconButton(
                                                tooltip: (_showPasswords
                                                        ? 'Hide'
                                                        : 'Show') +
                                                    ' passwords',
                                                icon: Icon(_showPasswords
                                                    ? Icons.visibility_off
                                                    : Icons.visibility),
                                                onPressed: () => setState(() =>
                                                    _showPasswords =
                                                        !_showPasswords),
                                              )),
                                          controller: passwordController,
                                          keyboardType:
                                              TextInputType.visiblePassword,
                                          obscureText: !_showPasswords,
                                        ),
                                      ),
                                    ],
                                  ),
                                  ButtonBar(
                                    children: [
                                      OutlineButton(
                                        child: Text('Import'),
                                        onPressed: () {
                                          if (_importFormKey.currentState
                                              .validate()) importSecretKey();
                                        },
                                      )
                                    ],
                                  )
                                ][i],
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
      ),
    );
  }

  Future<void> nextPage() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(
        'private_keys',
        jsonEncode([
          {'key': _secretKey, 'password': _secretKeyPassword}
        ]));
    Navigator.of(context).pushNamed('/inbox');
  }

  Future<void> initializeFormFields() async {
    //final prefs = await SharedPreferences.getInstance();
    /*pgpBlockController.text = prefs.getString('user') ?? '';
    passwordController.text = prefs.getString('password') ?? '';*/
  }

  setSecondaryPage(int i) {
    setState(() => _selectedPage = i);
    Future.delayed(
        Duration(milliseconds: 25),
        () => _secondaryPageController.animateToPage(i - 1,
            duration: Duration(milliseconds: 300), curve: Curves.easeInOut));
  }

  void generateSecretKey() {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Unfortunately, key generation is not possible yet.'),
    ));
    _secretKeyPassword = passwordController.text;
    throw UnimplementedError();
  }

  void importSecretKey() {
    _secretKey = pgpBlockController.text;
    _secretKeyPassword = passwordController.text;
    nextPage();
  }
}
