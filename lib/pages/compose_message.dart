import 'package:client/pages/login_page.dart';
import 'package:client/widgets/mailbox_tree_drawer.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chips_input/flutter_chips_input.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:responsive_scaffold/responsive_scaffold.dart';
import 'package:simple_gravatar/simple_gravatar.dart';
import 'package:zefyr/zefyr.dart';

showComposeDialog({@required BuildContext context, MessageComposeData data}) {
  MediaQuery.of(context).size.width < 1024
      ? Navigator.of(context).pushNamed(ComposePage.routeName)
      : showDialog(
          context: context,
          builder: (c) => AlertDialog(
                insetPadding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 10,
                    horizontal: MediaQuery.of(context).size.width / 10),
                title: Text(
                  'Compose',
                ),
                content: MessageComposer(),
              ));
}

class MessageComposeData {
  final List<MailAddress> to;
  final List<MailAddress> cc;
  final List<MailAddress> bcc;
  final MailAddress from;
  final String subject;
  final String body;
  final bool isHtml;

  MessageComposeData(
      {this.to,
      this.cc,
      this.bcc,
      this.from,
      this.subject,
      this.body,
      this.isHtml});
}

class MessageComposer extends StatefulWidget {
  @override
  _MessageComposerState createState() => _MessageComposerState();
}

/*List<MailAddress> contactMailAddresses = [
  MailAddress('John Doe', 'john.doe@secret.com')
];*/

class _MessageComposerState extends State<MessageComposer> {
  ZefyrController zefyrController = ZefyrController();
  FocusNode focusNode = FocusNode();
  List<MailAddress> contactMailAddresses = [];

  ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    importContacts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoScrollbar(
      controller: _scrollController,
      //isAlwaysShown: true,
      child: Container(
        width: MediaQuery.of(context).size.width > 768
            ? MediaQuery.of(context).size.width * 0.8
            : MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(32.0),
        child: ListView(
          controller: _scrollController,
          shrinkWrap: true,
          children: [
            ChipsInput(
              findSuggestions: (String query) {
                if (query.length != 0) {
                  var lowercaseQuery = query.toLowerCase();
                  List<MailAddress> addresses = contactMailAddresses
                      .where((profile) {
                    return profile.personalName
                            .toLowerCase()
                            .contains(query.toLowerCase()) ||
                        profile.email
                            .toLowerCase()
                            .contains(query.toLowerCase());
                  }).toList()
                        ..sort((a, b) => a.personalName
                            .toLowerCase()
                            .indexOf(lowercaseQuery)
                            .compareTo(b.personalName
                                .toLowerCase()
                                .indexOf(lowercaseQuery)));
                  if (RegExp(emailRegex).hasMatch(query)) {
                    addresses.insert(0, MailAddress(query, query));
                  }
                  return addresses;
                } else {
                  return const <MailAddress>[];
                }
              },
              onChanged: (addresses) {},
              chipBuilder: (context, state, MailAddress profile) {
                return InputChip(
                  key: ObjectKey(profile),
                  label: Text(profile.personalName),
                  avatar: CircleAvatar(
                    backgroundImage: NetworkImage(
                        Gravatar(profile.email.toLowerCase()).imageUrl(
                            size: 128, defaultImage: GravatarImage.identicon)),
                  ),
                  onDeleted: () => state.deleteChip(profile),
                  tooltip: profile.email,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                );
              },
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: 'To'),
              suggestionBuilder: (context, state, MailAddress profile) {
                return ListTile(
                  key: ObjectKey(profile),
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(
                        Gravatar(profile.email.toLowerCase()).imageUrl(
                            size: 128, defaultImage: GravatarImage.identicon)),
                  ),
                  title: Text(profile.personalName),
                  subtitle: Text(profile.email),
                  onTap: () => state.selectSuggestion(profile),
                );
              },
            ),
            CupertinoScrollbar(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: ZefyrToolbar.basic(
                  controller: zefyrController,
                ),
              ),
            ),
            ZefyrField(
              controller: zefyrController,
              focusNode: focusNode,
              //scrollable: true,
              //expands: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Message'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> importContacts() async {
    if (await Permission.contacts.request().isGranted) {
      Iterable<Contact> contacts = await ContactsService.getContacts();
      contacts.where((element) => element.emails.isNotEmpty).forEach((contact) {
        contact.emails.forEach((address) {
          contactMailAddresses
              .add(MailAddress(contact.displayName, address.value));
        });
      });
    }
  }
}

class ComposePage extends StatefulWidget {
  static const routeName = '/compose';

  @override
  _ComposePageState createState() => _ComposePageState();
}

class _ComposePageState extends State<ComposePage> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffold(
      drawer: MailboxTreeDrawer.current(),
      title: Text('Compose'),
      body: MessageComposer(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Send',
        child: Icon(Icons.send),
      ),
    );
  }
}
