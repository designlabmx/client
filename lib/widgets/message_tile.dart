import 'package:client/widgets/context_menu.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/material.dart';
import 'package:simple_gravatar/simple_gravatar.dart';

class MessageTile extends StatelessWidget {
  final MimeMessage message;
  final bool active;

  const MessageTile({Key key, this.message, this.active = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onSecondaryTapUp: (details) =>
          showContextMenu(details.globalPosition, context),
      onLongPressStart: (details) =>
          showContextMenu(details.globalPosition, context),
      child: Hero(
        tag: message,
        child: ListTile(
          //onLongPress: () {},
          tileColor:
              active ? Theme.of(context).primaryColor.withAlpha(128) : null,
          title: Tooltip(
            message: message.decodeSubject(),
            child: Text(
              message.decodeSubject(),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Image.network(
                Gravatar(message.decodeSender()[0].email.toLowerCase())
                    .imageUrl(
                        size: 128, defaultImage: GravatarImage.identicon)),
          ),
        ),
      ),
    );
  }

  void showContextMenu(Offset offset, BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => ContextMenu(position: offset, children: [
              ListTile(
                title: Text('Test Tile'),
                onTap: () {},
              )
            ]));
  }
}
