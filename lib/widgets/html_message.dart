import 'package:client/widgets/full_message_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

class HtmlMessage extends StatefulWidget {
  final String messageHtml;
  HtmlMessage(this.messageHtml, {Key key}) : super(key: key);

  @override
  _HtmlMessageState createState() => _HtmlMessageState();
}

class _HtmlMessageState extends State<HtmlMessage> {
/*  double _height = 400;
  static const String _kJavaScriptHeight =
      '''window.addEventListener('load', (event) => {
var scrollHeight = Math.max(
  //document.body.scrollHeight,
  //document.documentElement.scrollHeight,
  document.body.offsetHeight,
  document.documentElement.offsetHeight,
  document.body.clientHeight,
  //document.documentElement.clientHeight
);
sizeDetermination.postMessage(scrollHeight);
});
''';*/

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.messageHtml?.trim()?.isEmpty ?? true)
      return DefaultTextStyle.merge(
          child: Text(
            'No message body was found.',
          ),
          style: TextStyle(fontStyle: FontStyle.italic));
    return /*(Theme.of(context).platform == TargetPlatform.android ||
            Theme.of(context).platform == TargetPlatform.iOS)
        ? Container(
            height: _height,
            child: WebView(
              gestureNavigationEnabled: false,
              navigationDelegate: (nav) {
                launch(nav.url);
                return NavigationDecision.prevent;
              },
              javascriptChannels: Set.from([
                JavascriptChannel(
                    name: 'sizeDetermination',
                    onMessageReceived: (JavascriptMessage message) {
                      print(message.message);
                      setState(() {
                        _height = double.parse(message.message);
                      });
                    }),
              ]),
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (controller) {
                controller.loadUrl('data:text/html;base64,' +
                    base64Encode(Utf8Encoder().convert(widget.messageHtml)));
                Future.delayed(Duration(milliseconds: 250), () {
                  controller.evaluateJavascript(_kJavaScriptHeight);
                });
              },
            ))
        :*/
        Container(
      child: HtmlWidget(
        widget.messageHtml,
        onTapUrl: (url) {
          if (RegExp(
                  r'(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})')
              .hasMatch(url))
            showLinkPreview(url: url, context: context);
          else {
            Clipboard.setData(ClipboardData(text: url));
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Copied to clipboard.'),
            ));
          }
        },
      ),
    );
  }
}
