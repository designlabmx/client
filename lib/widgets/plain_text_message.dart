import 'dart:ui';

import 'package:client/widgets/full_message_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:selectable_autolink_text/selectable_autolink_text.dart';
import 'package:share/share.dart';

class PlainTextMessage extends StatelessWidget {
  final String message;

  const PlainTextMessage({Key key, this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (message?.trim()?.isEmpty ?? true)
      return DefaultTextStyle.merge(
          child: Text(
            'No message body was found.',
          ),
          style: TextStyle(fontStyle: FontStyle.italic));
    return SelectableAutoLinkText(
      message,
      style: TextStyle(fontFeatures: [FontFeature.tabularFigures()]),
      linkStyle: TextStyle(color: Colors.blueAccent),
      highlightedLinkStyle: TextStyle(
        color: Colors.blueAccent,
        backgroundColor: Colors.blueAccent.withAlpha(0x33),
      ),
      onTap: (url) {
        if (RegExp(
                r'(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})')
            .hasMatch(url))
          showLinkPreview(url: url, context: context);
        else {
          Clipboard.setData(ClipboardData(text: url));
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Copied to clipboard.'),
          ));
        }
      },
      onLongPress: (url) {
        try {
          Share.share(url);
        } catch (e) {
          Clipboard.setData(ClipboardData(text: url));
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Copied to clipboard.'),
          ));
        }
      },
    );
  }
}
