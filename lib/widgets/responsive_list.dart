import 'package:flutter/material.dart';

class ResponsiveList extends StatefulWidget {
  final List<ResponsiveListTile> children;
  final ScrollController scrollController;
  final PageController pageController;
  final Widget unselectedDetail;

  const ResponsiveList(
      {Key key,
      this.children,
      this.scrollController,
      this.unselectedDetail,
      this.pageController})
      : super(key: key);
  @override
  _ResponsiveListState createState() => _ResponsiveListState();
}

class _ResponsiveListState extends State<ResponsiveList> {
  bool isLargeScreen = false;

  Widget currentDetailPage;

  PageController controller = PageController();

  @override
  void initState() {
    resetSecondPage();
    if (widget.pageController != null) controller = widget.pageController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      if (MediaQuery.of(context).size.width >= 786) {
        isLargeScreen = true;
      } else {
        isLargeScreen = false;
      }

      final firstItem = /* Hero(
        child:*/
          ListView(
        shrinkWrap: true,
        controller: widget.scrollController,
        children: widget.children
            .map((e) => GestureDetector(
                  child: e,
                  onTap: () {
                    setState(() {
                      currentDetailPage = e.detailsBuilder(context);
                    });
                    if (!isLargeScreen)
                      controller.animateToPage(1,
                          duration: Duration(milliseconds: 300),
                          curve: Curves.easeInOut);
                  },
                ))
            .toList(),
        /*),
        tag: 'ResponsiveListFirstItem',*/
      );

      final secondItem = currentDetailPage;
      //Hero(child: currentDetailPage, tag: 'ResponsiveListSecondItem');

      Widget child;
      if (isLargeScreen) {
        child = Row(
          //mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: firstItem,
              flex: 2,
            ),
            Expanded(
              child: secondItem,
              flex: 3,
            )
          ],
        );
      } else
        child = PageView(
          onPageChanged: (newPage) {
            if (newPage == 0)
              setState(() {
                resetSecondPage();
              });
          },
          children: <Widget>[firstItem, secondItem],
          controller: controller,
        );
      return WillPopScope(
        child: child,
        onWillPop: () async {
          resetSecondPage();
          if (!isLargeScreen && controller.page == 1) {
            controller.animateToPage(0,
                duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
            return false;
          } else
            return true;
        },
      );
    });
  }

  void resetSecondPage() {
    currentDetailPage = widget.unselectedDetail;

    if (currentDetailPage == null) currentDetailPage = Container();
  }
}

class ResponsiveListTile extends StatelessWidget {
  final Widget child;
  final Function(BuildContext) detailsBuilder;

  const ResponsiveListTile({Key key, this.detailsBuilder, this.child})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return child;
  }
}
