import 'dart:typed_data';

import 'package:client/src/basic_syntax_highlighter.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:printing/printing.dart';

class MessageAttachmentTile extends StatelessWidget {
  final List<MimePart> parts;
  MessageAttachmentTile({@required this.parts});

  @override
  Widget build(BuildContext context) {
    List<MimePart> shortenedParts = List.from(parts)..removeAt(0);
    List<Widget> children = shortenedParts.map((MimePart e) {
      String filename = 'Unknown';
      final regex = RegExp(r'name="(.*)"');
      e.getHeader('Content-Type');

      if (regex.hasMatch(e.getHeader('Content-Type').first.value)) {
        filename = regex
            .allMatches(e.getHeader('Content-Type').first.value)
            .first
            .group(1);
      } else {
        e.headers.forEach((element) {
          print(element.name + ': ' + element.value);
          if (regex.hasMatch(element.value))
            filename = regex.allMatches(element.value).first.group(1);
        });
      }

      bool isPdf = e.mediaType.text.contains('pdf');

      bool isText = false;

      bool isImage = e.mediaType.isImage;

      try {
        isText = e.decodeTextPlainPart() != null;
      } catch (e) {}

      Uint8List contentBinary;
      try {
        contentBinary = Uint8List.fromList(e.decodeContentBinary());
      } catch (error) {
        contentBinary = Uint8List.fromList(e.decodeContentText().codeUnits);
      }

      return ListTile(
        title: Text(filename),
        leading: Icon(generateFileIcon(e)),
        trailing: isText || isPdf || isImage
            ? IconButton(
                icon: Icon(Icons.visibility),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (c) => AlertDialog(
                            title: Text(filename),
                            content: isPdf
                                ? Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.75,
                                    height: MediaQuery.of(context).size.height *
                                        0.75,
                                    child: PdfPreview(
                                      build: (d) => contentBinary,
                                      pdfFileName: filename,
                                    ),
                                  )
                                : isImage
                                    ? Container(
                                        child: InteractiveViewer(
                                            child: Image.memory(contentBinary)),
                                        constraints: BoxConstraints(
                                            minWidth: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                2 /
                                                3),
                                      )
                                    : SelectableText.rich(TextSpan(
                                        children: BasicSyntaxHighlighter()
                                            .parseText(e.decodeContentText()))),
                            actions: [
                              if (isText)
                                FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      Clipboard.setData(ClipboardData(
                                          text: e.decodeContentText()));
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                              content: Text(
                                                  'Copied to clipboard.')));
                                    },
                                    child: Text('Copy all')),
                              FlatButton(
                                  onPressed: () async {
                                    await saveFile(
                                        context: context,
                                        filename: filename,
                                        mediaType: e.mediaType.text,
                                        bytes: contentBinary);
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('Save as...')),
                              if (!isPdf)
                                FlatButton(
                                    onPressed: () async {
                                      isPdf
                                          ? await Printing.layoutPdf(
                                              onLayout: (f) => contentBinary,
                                              name: filename)
                                          : await Printing.layoutPdf(
                                              onLayout: (f) async =>
                                                  await Printing.convertHtml(
                                                      html: e
                                                          .decodeContentText()
                                                          .replaceAll(
                                                              '\n', '<br/>\n')),
                                              name: filename);
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('Print...')),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('Okay')),
                            ],
                          ));
                },
                tooltip: 'Show file',
              )
            : null,
        onTap: () => saveFile(
            context: context,
            filename: filename,
            mediaType: e.mediaType.text,
            bytes: contentBinary),
      );
    }).toList();
    return ExpansionTile(
        initiallyExpanded: true,
        title: Text('This message has attachments.'),
        leading: Icon(Icons.attach_file),
        children: children);
  }

  Future saveFile(
      {BuildContext context,
      Uint8List bytes,
      String filename,
      String mediaType}) {
    return FilePickerCross(bytes,
            type: FileTypeCross.custom,
            fileExtension: mediaType,
            path: filename)
        .exportToStorage()
        .then((value) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Saved to $value')));
    }).catchError((e) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('You canceled the saving process.')));
    });
  }

  IconData generateFileIcon(MimePart part) {
    if (part.mediaType.text.contains('pgp')) return Icons.vpn_key;
    if (part.mediaType.text.contains('pdf')) return Icons.picture_as_pdf;
    if (part.mediaType.isImage) return Icons.image;
    if (part.mediaType.isAudio) return Icons.audiotrack;
    if (part.mediaType.isFont) return Icons.text_fields;
    if (part.mediaType.isMessage) return Icons.message;
    if (part.mediaType.isText) return Icons.text_snippet;
    if (part.mediaType.isVideo) return Icons.ondemand_video;
    if (part.mediaType.isApplication) return Icons.settings_applications;
    return Icons.insert_drive_file;
  }
}
