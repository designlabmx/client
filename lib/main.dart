import 'package:client/pages/compose_message.dart';
import 'package:client/pages/inbox_page.dart';
import 'package:client/pages/key_setup_page.dart';
import 'package:client/pages/login_page.dart';
import 'package:client/pages/splash_screen.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';

void main() {
  /*SharedPreferences.setMockInitialValues(
      {'user': 'user@example.com', 'password': 'password'});*/
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => brightness == Brightness.light
          ? ThemeData(
              fontFamily: 'Jost',
              primaryColor: Colors.green,
              accentColor: Colors.pink,
              brightness: Brightness.light,
              cardTheme: CardTheme(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  margin: EdgeInsets.all(8)),
            )
          : ThemeData(
              fontFamily: 'Jost',
              primaryColor: Colors.green,
              accentColor: Colors.pink,
              brightness: Brightness.dark),
      themedWidgetBuilder: (context, theme) => MaterialApp(
        title: 'EXTINCTION.EMAIL',
        theme: theme,
        initialRoute: SplashScreen.routeName,
        routes: {
          SplashScreen.routeName: (c) => SplashScreen(),
          InboxPage.routeName: (c) => InboxPage(),
          LoginPage.routeName: (c) => LoginPage(),
          KeySetupPage.routeName: (c) => KeySetupPage(),
          ComposePage.routeName: (c) => ComposePage(),
        },
      ),
    );
  }
}
