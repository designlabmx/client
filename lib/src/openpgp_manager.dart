import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

@JsonSerializable(nullable: false)
class OpenPGPManager {
  static String _jsonKey = 'OpenPGPManager';
  static String _fallbackJson = '{}';
  OpenPGPManager(
      {Iterable<PrivateKey> privateKeys, Iterable<PublicKey> publicKeys}) {
    _privateKeys = privateKeys.toSet();
    _publicKeys = publicKeys.toSet();
  }

  static FutureOr<OpenPGPManager> getInstance() {
    if (_current != null) return _current;
    return SharedPreferences.getInstance().then((prefs) =>
        OpenPGPManager.fromJson(
            jsonDecode(prefs.getString(_jsonKey) ?? _fallbackJson)));
  }

  static OpenPGPManager _current;

  Set<PrivateKey> _privateKeys;
  Set<PublicKey> _publicKeys;

  Set<PublicKey> get publicKeys => _publicKeys;
  Set<PrivateKey> get privateKeys => _privateKeys;

  void addPrivateKey(PrivateKey key) {
    _privateKeys.add(key);
    save();
  }

  void deletePrivateKey(PrivateKey key) {
    _privateKeys.remove(key);
    save();
  }

  void addPublicKey(PublicKey key) {
    _publicKeys.add(key);
    save();
  }

  void deletePublicKey(PublicKey key) {
    _publicKeys.remove(key);
    save();
  }

  Future<List<RemoteKey>> findRemoteKeys(String query) async {
    return [];
  }

  Future<bool> importRemoteKey(RemoteKey key) async {
    save();
    return true;
  }

  Future<bool> save() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString(_jsonKey, jsonEncode(toJson()));
  }

  factory OpenPGPManager.fromJson(Map<String, dynamic> json) =>
      _$OpenPGPManagerFromJson(json);
  Map<String, dynamic> toJson() => _$OpenPGPManagerToJson(this);
}

class PublicKey {
  final String key;

  PublicKey({@required this.key});
  @override
  String toString() {
    return key;
  }
}

class PrivateKey {
  final String key;

  PrivateKey({@required this.key});
  @override
  String toString() {
    return key;
  }
}

class RemoteKey {
  final String key;

  RemoteKey({@required this.key});

  @override
  String toString() {
    return key;
  }
}

OpenPGPManager _$OpenPGPManagerFromJson(Map<String, dynamic> json) {
  return OpenPGPManager(
    publicKeys:
        (json['publicKeys'] as Iterable<String>).map((e) => PublicKey(key: e)),
    privateKeys: (json['privateKeys'] as Iterable<String>)
        .map((e) => PrivateKey(key: e)),
  );
}

Map<String, dynamic> _$OpenPGPManagerToJson(OpenPGPManager instance) =>
    <String, dynamic>{
      'publicKeys': instance.publicKeys,
      'privateKeys': instance.privateKeys,
    };
